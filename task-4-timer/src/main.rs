#![deny(clippy::all, clippy::pedantic, unsafe_code)]

use std::time::{Duration, Instant};

use iced::{
    executor,
    widget::{button, column, container, progress_bar, row, slider, text},
    Alignment, Application, Command, Length, Settings, Theme,
};

fn main() -> iced::Result {
    Task::run(Settings::default())
}

#[derive(Clone, Debug)]
enum Message {
    DurationChange(f32),
    ResetPressed,
    Tick(Instant),
}

struct Task {
    duration: Duration,
    elapsed: Duration,
    start: Instant,
}
impl Default for Task {
    fn default() -> Self {
        Self {
            duration: Duration::from_secs(5),
            elapsed: Duration::default(),
            start: Instant::now(),
        }
    }
}

impl Application for Task {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        (Task::default(), Command::none())
    }

    fn subscription(&self) -> iced::Subscription<Self::Message> {
        if self.elapsed >= self.duration {
            // 7GUIs: When e ≥ d is true then the timer stops (and G will be full).
            iced::Subscription::none()
        } else {
            iced::time::every(Duration::from_millis(200)).map(Message::Tick)
        }
    }

    fn title(&self) -> String {
        String::from("task")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::DurationChange(d) => {
                self.duration = Duration::from_secs_f32(d);
            }
            Message::ResetPressed => {
                // 7GUIs: Clicking R will reset e to zero.
                let Self {
                    duration,
                    elapsed,
                    start,
                } = Self::default();
                self.duration = duration;
                self.elapsed = elapsed;
                self.start = start;
            }
            Message::Tick(_) => self.elapsed = self.start.elapsed(),
        }
        Command::none()
    }

    fn view(&self) -> iced::Element<'_, Self::Message, iced::Renderer<Self::Theme>> {
        let gauge = row![
            text("elapsed time:"),
            progress_bar(
                0.0..=self.duration.as_secs_f32(),
                self.elapsed.as_secs_f32()
            )
            .width(180)
        ]
        .align_items(Alignment::Center)
        .padding(8)
        .spacing(8);

        let elapsed = text(format!("{:.1}s", self.elapsed.as_secs_f32())).size(32);

        let duration = row![
            text("duration:"),
            slider(
                0.0..=10.0,
                self.duration.as_secs_f32(),
                Message::DurationChange
            )
            .step(0.1)
            .width(180)
        ]
        .align_items(Alignment::Center)
        .padding(8)
        .spacing(8);

        let reset = button(text("reset")).on_press(Message::ResetPressed);

        container(
            column![gauge, elapsed, duration, reset]
                .align_items(Alignment::Center)
                .padding(8)
                .spacing(8),
        )
        .center_x()
        .center_y()
        .height(Length::Fill)
        .width(Length::Fill)
        .into()
    }
}
