#![deny(clippy::all, clippy::pedantic, unsafe_code)]

use std::fmt::Display;

use chrono::{Local, NaiveDate, ParseResult};
use iced::{
    executor,
    widget::{button, column, container, pick_list, text},
    Alignment, Application, Color, Command, Length, Settings, Theme,
};

fn main() -> iced::Result {
    Task::run(Settings::default())
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
enum Direction {
    #[default]
    OneWay,
    ReturnFlight,
}
impl Display for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::OneWay => "one-way flight",
                Self::ReturnFlight => "return flight",
            }
        )
    }
}

#[derive(Clone, Debug)]
enum Message {
    BookPressed,
    DirectionSelected(Direction),
    ReturnDateInput(String),
    StartDateInput(String),
}

#[derive(Debug)]
struct Task {
    direction: Direction,
    return_date: ParseResult<NaiveDate>,
    return_date_raw: String,
    start_date: ParseResult<NaiveDate>,
    start_date_raw: String,
    status: String,
}
impl Default for Task {
    fn default() -> Self {
        let now = Local::now();
        let datetime = now.naive_local().date();
        Self {
            direction: Direction::default(),
            return_date: Ok(datetime),
            return_date_raw: String::from("2023-04-01"),
            start_date: Ok(datetime),
            start_date_raw: String::from("2023-04-01"),
            status: String::new(),
        }
    }
}

impl Application for Task {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        (Task::default(), Command::none())
    }

    fn title(&self) -> String {
        String::from("task")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::BookPressed => {
                // When clicking B a message is displayed informing the user of his selection (e.g. “You have booked a one-way flight on 04.04.2014.”).
                self.status = format!(
                    "you have booked a {} on {}",
                    &self.direction, &self.start_date_raw
                );
            }
            Message::DirectionSelected(d) => self.direction = d,
            Message::ReturnDateInput(s) => {
                self.return_date = NaiveDate::parse_from_str(s.as_str(), "%Y-%m-%d");
                self.return_date_raw = s;
            }
            Message::StartDateInput(s) => {
                self.start_date = NaiveDate::parse_from_str(s.as_str(), "%Y-%m-%d");
                self.start_date_raw = s;
            }
        }
        Command::none()
    }

    fn view(&self) -> iced::Element<'_, Self::Message, iced::Renderer<Self::Theme>> {
        let direction = pick_list::<'_, Self::Message, iced::Renderer<Self::Theme>, Direction>(
            vec![Direction::OneWay, Direction::ReturnFlight],
            Some(self.direction),
            Message::DirectionSelected,
        );
        let mut r#return = iced::widget::text_input("yyyy-mm-dd", &self.return_date_raw).width(160);
        if self.direction == Direction::ReturnFlight {
            // 7GUI: T2 is enabled iff C’s value is “return flight”.
            r#return = r#return.on_input(Message::ReturnDateInput);
            if !self.return_date_raw.is_empty() && self.return_date.is_err() {
                // 7GUI: When a non-disabled textfield T has an ill-formatted date then T is colored red and B is disabled.
                r#return =
                    r#return.style(iced::theme::TextInput::Custom(Box::new(InvalidInputTheme)));
            }
        }

        let mut start = iced::widget::text_input("yyyy-mm-dd", &self.start_date_raw)
            .on_input(Message::StartDateInput)
            .width(160);
        if !self.start_date_raw.is_empty() && self.start_date.is_err() {
            // 7GUI: When a non-disabled textfield T has an ill-formatted date then T is colored red and B is disabled.
            start = start.style(iced::theme::TextInput::Custom(Box::new(InvalidInputTheme)));
        }

        let status = text(&self.status);

        let mut book = button(text("book"));
        match (&self.direction, &self.start_date, &self.return_date) {
            (Direction::OneWay, Ok(_start_date), _) => {
                book = book.on_press(Message::BookPressed);
            }
            (Direction::ReturnFlight, Ok(start_date), Ok(return_date))
                if start_date <= return_date =>
            {
                book = book.on_press(Message::BookPressed);
            }
            _ => {
                // 7GUI: When C has the value “return flight” and T2’s date is strictly before T1’s then B is disabled.
                // 7GUI: When a non-disabled textfield T has an ill-formatted date then T is colored red and B is disabled.
            }
        }

        container(
            column![direction, start, r#return, book, status]
                .align_items(Alignment::Center)
                .padding(8)
                .spacing(8),
        )
        .center_x()
        .center_y()
        .height(Length::Fill)
        .width(Length::Fill)
        .into()
    }
}

#[derive(Debug)]
struct InvalidInputTheme;
impl iced::widget::text_input::StyleSheet for InvalidInputTheme {
    /// Style here is the associated Theme type from the Application,
    /// which usually will be `iced::theme::Theme` with variants Light, Dark, and Custom
    type Style = iced::theme::Theme;

    fn active(&self, style: &Self::Style) -> iced::widget::text_input::Appearance {
        iced::widget::text_input::Appearance {
            background: iced::Background::Color(style.palette().danger),
            ..style.active(&iced::theme::TextInput::Default)
        }
    }

    fn focused(&self, style: &Self::Style) -> iced::widget::text_input::Appearance {
        iced::widget::text_input::Appearance {
            background: iced::Background::Color(style.palette().danger),
            ..style.focused(&iced::theme::TextInput::Default)
        }
    }

    fn placeholder_color(&self, style: &Self::Style) -> Color {
        style.extended_palette().background.strong.color
    }

    fn value_color(&self, style: &Self::Style) -> Color {
        style.extended_palette().background.base.text
    }

    fn disabled_color(&self, style: &Self::Style) -> Color {
        style.extended_palette().background.weak.color
    }

    fn selection_color(&self, style: &Self::Style) -> Color {
        style.extended_palette().primary.weak.color
    }

    fn disabled(&self, style: &Self::Style) -> iced::widget::text_input::Appearance {
        iced::widget::text_input::Appearance {
            ..style.disabled(&iced::theme::TextInput::Default)
        }
    }
}
