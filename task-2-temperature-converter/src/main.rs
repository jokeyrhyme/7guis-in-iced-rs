#![deny(clippy::all, clippy::pedantic, unsafe_code)]

use iced::{
    executor,
    widget::{container, row, text, text_input},
    Alignment, Application, Command, Length, Settings, Theme,
};

fn main() -> iced::Result {
    Task::run(Settings::default())
}

#[derive(Clone, Debug)]
enum Message {
    CelsiusChanged(String),
    FahrenheitChanged(String),
}

#[derive(Default)]
struct Task {
    celsius: Option<i32>,
    fahrenheit: Option<i32>,
}

impl Application for Task {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        (Task::default(), Command::none())
    }

    fn title(&self) -> String {
        String::from("task")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::CelsiusChanged(cs) => {
                if cs.trim().is_empty() {
                    self.celsius = None;
                    self.fahrenheit = None;
                }

                #[allow(clippy::cast_possible_truncation)]
                if let Ok(cu) = cs.parse::<i32>() {
                    self.celsius = Some(cu);
                    let f_fp = f64::try_from(cu).unwrap_or_default() * (9.0 / 5.0) + 32.0;
                    self.fahrenheit = Some(f_fp as f32 as i32);
                }
            }
            Message::FahrenheitChanged(fs) => {
                if fs.trim().is_empty() {
                    self.celsius = None;
                    self.fahrenheit = None;
                }

                #[allow(clippy::cast_possible_truncation)]
                if let Ok(fu) = fs.parse::<i32>() {
                    let c_fp: f64 = (f64::try_from(fu).unwrap_or_default() - 32.0) * (5.0 / 9.0);
                    self.celsius = Some(c_fp as f32 as i32);
                    self.fahrenheit = Some(fu);
                }
            }
        }
        Command::none()
    }

    fn view(&self) -> iced::Element<'_, Self::Message, iced::Renderer<Self::Theme>> {
        let celsius_s = if let Some(cu) = &self.celsius {
            format!("{cu}")
        } else {
            String::new()
        };
        let celsius = text_input("", celsius_s.as_str())
            .on_input(Message::CelsiusChanged)
            .width(120);
        let celsius_label = text("celsius");

        let equals_label = text("=");

        let fahrenheit_s = if let Some(fu) = &self.fahrenheit {
            format!("{fu}")
        } else {
            String::new()
        };
        let fahrenheit = text_input("", fahrenheit_s.as_str())
            .on_input(Message::FahrenheitChanged)
            .width(120);
        let fahrenheit_label = text("fahrenheit");

        container(
            row![
                celsius,
                celsius_label,
                equals_label,
                fahrenheit,
                fahrenheit_label
            ]
            .align_items(Alignment::Center)
            .padding(8)
            .spacing(8),
        )
        .center_x()
        .center_y()
        .height(Length::Fill)
        .width(Length::Fill)
        .into()
    }
}
