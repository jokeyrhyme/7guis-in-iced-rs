# 7GUIs in iced-rs

teaching myself GUIs in Rust with iced-rs via "The 7 Tasks" from 7GUIs

## see also

- https://7guis.github.io/7guis/tasks/
- https://github.com/7guis/7guis
- https://github.com/iced-rs/iced
