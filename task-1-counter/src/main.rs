#![deny(clippy::all, clippy::pedantic, unsafe_code)]

use iced::{
    executor,
    widget::{button, container, row, text},
    Alignment, Application, Command, Length, Settings, Theme,
};

fn main() -> iced::Result {
    Task::run(Settings::default())
}

#[derive(Clone, Debug)]
enum Message {
    Increment,
}

#[derive(Default)]
struct Task {
    counter: u32,
}

impl Application for Task {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        (Task::default(), Command::none())
    }

    fn title(&self) -> String {
        String::from("task")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::Increment => {
                self.counter += 1;
                Command::none()
            }
        }
    }

    fn view(&self) -> iced::Element<'_, Self::Message, iced::Renderer<Self::Theme>> {
        let output = text(format!("{}", &self.counter));

        let count = button(text("count")).on_press(Message::Increment);

        container(
            row![output, count]
                .align_items(Alignment::Center)
                .padding(8)
                .spacing(8),
        )
        .center_x()
        .center_y()
        .height(Length::Fill)
        .width(Length::Fill)
        .into()
    }
}
