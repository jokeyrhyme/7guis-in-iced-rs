#![deny(clippy::all, clippy::pedantic, unsafe_code)]

use std::fmt;

use iced::{
    executor,
    widget::{button, column, container, radio, row, text, text_input, Column},
    Alignment, Application, Command, Length, Settings, Theme,
};

fn main() -> iced::Result {
    Task::run(Settings::default())
}

#[derive(Clone, Debug)]
enum Message {
    CreatePressed,
    DeletePressed,
    NameInput(String),
    PrefixInput(String),
    RecordSelected(usize),
    SurnameInput(String),
    UpdatePressed,
}

#[derive(Clone, Debug, Default)]
struct Record {
    name: String,
    surname: String,
}
impl Record {
    fn is_empty(&self) -> bool {
        self.name.is_empty() || self.surname.is_empty()
    }
}
impl fmt::Display for Record {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}, {}", self.surname, self.name)
    }
}

#[derive(Default)]
struct Task {
    input: Record,
    records: Vec<Record>,
    prefix: String,
    selected: Option<usize>,
}

impl Application for Task {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        (Task::default(), Command::none())
    }

    fn title(&self) -> String {
        String::from("task")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::CreatePressed => {
                // 7GUIs: Clicking BC will append the resulting name from concatenating the strings in Tname and Tsurname to L
                let mut new = Record::default();
                std::mem::swap(&mut new, &mut self.input);
                self.records.push(new);
                self.selected = None;
            }
            Message::DeletePressed => {
                // 7GUIs: BD will remove the selected entry.
                if let Some(i) = self.selected {
                    self.records.remove(i);
                }
                self.selected = None;
            }
            Message::NameInput(s) => self.input.name = s,
            Message::PrefixInput(s) => {
                self.prefix = s;
                self.selected = None;
            }
            Message::RecordSelected(i) => {
                self.input.name = self.records[i].name.clone();
                self.input.surname = self.records[i].surname.clone();
                self.selected = Some(i);
            }
            Message::SurnameInput(s) => self.input.surname = s,
            Message::UpdatePressed => {
                // 7GUIs: In contrast to BC,
                // BU will not append the resulting name but instead replace the selected entry with the new name.
                if let Some(i) = self.selected {
                    if !self.input.is_empty() {
                        self.records[i] = self.input.clone();
                    }
                }
            }
        }
        Command::none()
    }

    fn view(&self) -> iced::Element<'_, Self::Message, iced::Renderer<Self::Theme>> {
        let prefix = text_input("", &self.prefix)
            .on_input(Message::PrefixInput)
            .width(64);
        let filters = row![text("filter prefix:"), prefix]
            .align_items(Alignment::Center)
            .padding(8)
            .spacing(8);

        let name = text_input("", &self.input.name)
            .on_input(Message::NameInput)
            .width(128);
        let surname = text_input("", &self.input.surname)
            .on_input(Message::SurnameInput)
            .width(128);

        // TODO: probably need to implement a bespoke widget for this
        let mut data: Column<_, _> = column![];
        for (i, record) in self.records.iter().enumerate() {
            if record.surname.starts_with(&self.prefix) {
                data = data.push(radio(
                    format!("{record}"),
                    i,
                    self.selected,
                    Message::RecordSelected,
                ));
            }
        }

        let mut create = button(text("create"));
        if !self.input.is_empty() {
            create = create.on_press(Message::CreatePressed);
        }
        let mut update = button(text("update"));
        if !self.input.is_empty() && self.selected.is_some() {
            // 7GUIs: BU and BD are enabled iff an entry in L is selected
            update = update.on_press(Message::UpdatePressed);
        }
        let mut delete = button(text("delete"));
        if self.selected.is_some() {
            // 7GUIs: BU and BD are enabled iff an entry in L is selected
            delete = delete.on_press(Message::DeletePressed);
        }

        let buttons = row![create, update, delete]
            .align_items(Alignment::Center)
            .padding(8)
            .spacing(8);

        container(
            column![
                row![
                    column![filters, data.height(320)].padding(8),
                    column![
                        row![text("name:").width(72), name]
                            .align_items(Alignment::Center)
                            .padding(4),
                        row![text("surname:").width(72), surname]
                            .align_items(Alignment::Center)
                            .padding(4)
                    ]
                    .padding(8)
                ]
                .align_items(Alignment::Center),
                buttons
            ]
            .align_items(Alignment::Center)
            .padding(8)
            .spacing(8),
        )
        .center_x()
        .center_y()
        .height(Length::Fill)
        .width(Length::Fill)
        .into()
    }
}
